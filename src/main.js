/*
 * @Author: Nend 758258389@qq.com
 * @Date: 2022-10-26 14:29:15
 * @LastEditors: Nend 758258389@qq.com
 * @LastEditTime: 2022-10-31 15:11:12
 * @FilePath: \phase4-front\src\main.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'
import API from './assets/config/api'
import BaiduMap from 'vue-baidu-map';
import animated from 'animate.css'

Vue.prototype.API = API
Vue.use(BaiduMap, {
  /* Visit http://lbsyun.baidu.com/apiconsole/key for details about app key. */
  ak: 'hBoavuO4jDezgivYa9d7MoYq4kaEz5Ci'
})
Vue.use(animated)
Vue.config.productionTip = false


// N 请求拦截器 -----------------------------------------------------------------

axios.interceptors.request.use(
  
  // N 在请求之前做些什么  配置统一请求头、异步请求
  function(config){
    // N 获取请求路径
    let pathname = location.pathname;
    console.log(pathname);

    // console.log("以下为请求前增强后的内容 config");

    // config.headers['Content-Type'] = "application/json;charset=UTF-8";
    // config.headers['X-Requested-With'] = "XMLHttpRequest";
    
    // console.log(config);
    
    // !! N 非首页非登录页面拦截 
    if(pathname != '/' || pathname != '/login' || '/orderInfo/order/saveOrder'){
      if(localStorage.getItem("x-token") || localStorage.getItem("x-token")!=""){
        return config;
      }
         // N 如果sessionStorage没有token，跳去首页
         else{
          router.replace({
            path: '/'
            // ** 登录成功后跳入浏览的当前页面
            // query: {redirect: router.currentRoute.fullPath}
        });
     }
    }else{
      return config;
    }
  },

  // 对请求错误做什么
  function(error){
    Vue.prototype.$message.error('请求超时,请重新登录!!!');
    return Promise.reject(error);
  },
)


// N 响应拦截器 -----------------------------------------------------------------

axios.interceptors.response.use(
  resp => {




    // !! N jwt验证异常 前端响应处理
    if(resp.data.code == "411"){
      Vue.prototype.$message.error("未登录状态，请先登录");
      router.replace({
        path: '/'
        // ** 登录成功后跳入浏览的当前页面
        // query: {redirect: router.currentRoute.fullPath}
    });
    }
    if(resp.data.code == "412"){
      Vue.prototype.$message.error("令牌过期，请重新登录");
      router.replace({
        path: '/'
        // ** 登录成功后跳入浏览的当前页面
        // query: {redirect: router.currentRoute.fullPath}
    });
    }
    if(resp.data.code == "413"){
      Vue.prototype.$message.error("令牌无效，请重新登录");
      router.replace({
        path: '/'
        // ** 登录成功后跳入浏览的当前页面
        // query: {redirect: router.currentRoute.fullPath}
    });
    }
    if(resp.data.code == "414"){
      Vue.prototype.$message.error("令牌为空，请重新登录");
      router.replace({
        path: '/'
        // ** 登录成功后跳入浏览的当前页面
        // query: {redirect: router.currentRoute.fullPath}
    });
    }

    // !! 登录异常 前端响应处理
    if(resp.data.code == "500"){
      Vue.prototype.$message.error("账号或密码错误");
      router.replace({
        path: '/'
        // ** 登录成功后跳入浏览的当前页面
        // query: {redirect: router.currentRoute.fullPath}
    });
    }
    console.log("下面单个响应内容")
    console.log(resp);

    /**
     * !! N 统一在localStorage 进行jwt存储
     */
    if(resp.headers['x-token']!=null && resp.headers['x-token']!=""){
      window.localStorage.setItem("x-token",resp.headers['x-token']);
    }


    return resp;
  },

  // N 默认的异常处理，我们使用响应的内容进行判断
  (err) => {
    try {
      // 返回接口的错误信息
      return Promise.reject(err.response.data);
    } catch (e) {
      return Promise.reject(err);
    }
  }
);


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
