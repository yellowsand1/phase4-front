/*
 * @Author: Nend 758258389@qq.com
 * @Date: 2022-10-26 14:29:15
 * @LastEditors: Nend 758258389@qq.com
 * @LastEditTime: 2022-10-27 19:47:54
 * @FilePath: \phase4-front\src\store\index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import Vue from 'vue'
import Vuex from 'vuex'
import tab from './tab'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userInfo:[],
    isLogin:false,
    user_x_token: "",
    admin_x_token:"",
    isLogout: true,
    // 字体显示
    text1: "普通登录",
  },
  mutations: {//存全局的同步方法
    getInitData(state){

    },
  },
  actions: {
  },
  modules: {
    tab
  }
})
