/*
 * @Author: Nend 758258389@qq.com
 * @Date: 2022-10-26 16:48:44
 * @LastEditors: Nend 758258389@qq.com
 * @LastEditTime: 2022-10-29 15:46:50
 * @FilePath: \phase4-front\vue.config.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// 跨域配置

module.exports = {
    devServer: {
        proxy: { // 配置跨域

            // 网关
            '/api': {
                // ws: true,        //如果要代理 websockets，配置这个参数
                // secure: false,  // 如果是https接口，需要配置这个参数
                target: `http://localhost:8080/`, //请求后台接口
                changeOrigin: true, // 允许跨域
                pathRewrite: {
                    '^/api': '' // 重写请求,默认为/api，以后的请求直接使用/api就可以访问代理地址
                }
            },


            // 单个配置: 局域网网关
            '/gapi': {
                // ws: true,        //如果要代理 websockets，配置这个参数
                // secure: false,  // 如果是https接口，需要配置这个参数
                target: `http://192.168.12.58:8080/`, //请求后台接口
                changeOrigin: true, // 允许跨域
                // pathRewrite: {
                //     '^/userTest': '' // 重写请求,默认为/api，以后的请求直接使用/api就可以访问代理地址
                // }
            },

            
            // 单个配置: 内置
            '/napi': {
                // ws: true,        //如果要代理 websockets，配置这个参数
                // secure: false,  // 如果是https接口，需要配置这个参数
                target: `http://localhost:8081/`, //请求后台接口
                changeOrigin: true, // 允许跨域
                // pathRewrite: {
                //     '^/userTest': '' // 重写请求,默认为/api，以后的请求直接使用/api就可以访问代理地址
                // }
            },
        },
    }
}